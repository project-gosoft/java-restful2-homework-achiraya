package com.test.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.test.demo.model.Manager;

public interface ManagerRepository extends CrudRepository<Manager, Integer> {

}
