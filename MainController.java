package com.test.demo.controller;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.test.demo.exception.InputFielException;
import com.test.demo.model.Manager;
import com.test.demo.model.Shop;
import com.test.demo.repo.ManagerRepository;
import com.test.demo.repo.ShopRepository;

@Controller // This means that this class is a Controller
public class MainController {
	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello World!";
	}

	private static final Logger logger = Logger.getLogger("InputFieldException.class");
	FileHandler fh;

	@Autowired
	private ShopRepository shopRepository;
	@Autowired
	private ManagerRepository managerRepository;

	@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Input field is wrong") // 504
	@ExceptionHandler(InputFielException.class)
	public void handleInputFieldException() {
		try {

			// This block configure the logger with handler and formatter
			fh = new FileHandler("C:/Users/ACHI/Desktop/Java_Restful/MyLogFile.log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

			// the following statement is used to log any messages
			logger.info("My first log");

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		logger.info("Hi How r u?");
		System.out.println("Handle Input field Exception");

		// logger.debug("This is debug");
		logger.info("This is info");
		// logger.warn("This is warn");
		// logger.error("This is error");
	}
	
		//----shop---//
	@GetMapping(path = "/shop")
	public @ResponseBody Iterable<Shop> getAllUsers1() {
		return shopRepository.findAll();
	}

	@PostMapping(path = "/shop/add") 
	public @ResponseBody String addNewUser(@RequestParam String shop, @RequestParam String computer,
			@RequestParam String staff, @RequestParam int costOfCom) {
		Shop n = new Shop();
		n.setShop(n.getShop());
		n.setComputer(n.getComputer());
		n.setStaff(n.getStaff());
		n.setCostOfCom(n.getCostOfCom());
		shopRepository.save(n);
		return "Saved";
	}

	@PutMapping(path = "shop/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editShop(@RequestBody Shop shop) throws InputFielException {
		if (shop.getCostOfCom() < 1500) {
			throw new InputFielException(false, false, false, false, true);
		}

		shop.setShop(shop.getShop());
		shopRepository.save(shop);
		return "Saved";
	}

	@DeleteMapping("/shop/delete/{shop_id}")
	@ResponseBody
	String delete(@PathVariable("shop_id") int shopId) {
		if (!shopRepository.findById(shopId).isEmpty()) {
			shopRepository.deleteById(shopId);
		}
		return "Delete shop :" + shopId;
	}

	//------Computer-----//

	@GetMapping(path = "/computer")
	public @ResponseBody Iterable<Shop> getAllUsers() {
		return shopRepository.findAll();
	}

	@PostMapping(path = "/computer/add") // Map ONLY POST Requests
	public @ResponseBody String addNewUsers(@RequestParam String shop, @RequestParam String computer,
			@RequestParam String staff, @RequestParam String manager) {

		Shop n = new Shop();
		Manager m = new Manager();
		n.setShop(shop);
		n.setComputer(computer);
		n.setStaff(staff);
		m.setUsername(manager);
		n.setManager(m);
		shopRepository.save(n);
		managerRepository.save(m);
		return "Saved";
	}

	@PutMapping(path = "computer/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editComputer(@RequestBody Shop computer) {
		computer.setShop(computer.getShop());
		shopRepository.save(computer);
		return "Saved";
	}

	@DeleteMapping(path = "/delete/{id}")
	@ResponseBody
	public String delete1(@PathVariable("id") int shopId) {
		if (!shopRepository.findById(shopId).isEmpty()) {
			shopRepository.deleteById(shopId);
		}
		return "Delete shop :" + shopId;
	}

	//----Manager----//

	@GetMapping(path = "/manager")
	public @ResponseBody Iterable<Manager> getAllUsers2() {
		return managerRepository.findAll();
	}

	@PostMapping(path = "/manager/add") // Map ONLY POST Requests
	public @ResponseBody String addManager(@RequestParam String manager) {
		Manager n = new Manager();
		n.setUsername(manager);
		managerRepository.save(n);
		return "Saved";
	}

	@PutMapping(path = "manager/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editManager(@RequestBody Manager manager) {
		manager.setUsername(manager.getUsername());
		managerRepository.save(manager);
		return "Saved";
	}

	@DeleteMapping(path = "manager/delete/{managerId}")
	@ResponseBody
	public String deleteManager(@PathVariable("managerId") int id) {
		if (!managerRepository.findById(id).isEmpty()) {
			managerRepository.deleteById(id);
		}
		return "Delete manager :" + id;
	}
}
