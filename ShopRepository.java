package com.test.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.test.demo.model.Shop;

public interface ShopRepository extends CrudRepository<Shop, Integer> {

}
